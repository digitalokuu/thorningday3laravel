<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Crud;

//NOTE: INDEX
Route::get('/',function(){
    return view('welcome');
})->name('crud.CrudList');

//NOTE: Detailed view
Route::get('View/{id}','CrudController@ViewDetailed',function($id){})->name('crud.CrudViewDetailed');

//NOTE: CrudController -> List(), link : crud.CrudList (OutputList)
Route::get('List','CrudController@List')->name('crud.CrudList');

//NOTE: CrudController -> Create(), link : crud.CrudCreate (Input page)
Route::get('Create','CrudController@Create')->name('crud.CrudCreate');

//NOTE: CrudController -> Create(), link : crud.CrudCreatePost (Input Process)
Route::post('Create','CrudController@CreatePost')->name('crud.CrudCreatePost');

//NOTE: CrudController -> Edit(), link : crud.CrudCreate (Edit page)
Route::get('Edit/{id}','CrudController@Edit',function($id){})->name('crud.CrudEdit');

//NOTE: CrudController -> Edit(), link : crud.CrudCreate (Edit Process)
Route::post('Edit','CrudController@EditPost')->name('crud.CrudEditPost');

//NOTE: CrudController -> Delete(), link : crud.CrudDeletePost (Delete Process)
Route::get('Delete/{id}','CrudController@DeletePost',function($id){})->name('crud.CrudDeletePost');

//NOTE: CrudController -> Delete(), link : crud.CrudDeletePost (Delete Process)
Route::get('View/{id}/komentar','CrudController@abc')->name('crud.CrudMentar');
// Route::get('View/{$id}/komentar',function(){
//     return 'halo';
// })->name('crud.CrudMentar');

Route::post('View/{id}/komentar','CrudController@Store_Comment')->name('crud.CrudMentar.store');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
