<?php

namespace App\Http\Controllers;

use League\Flysystem\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\crudreq;
use App\Crud;
use App\Comment;
use Session;
use Image;
// use Input;

class CrudController extends Controller
{
    public function ViewDetailed(Request $request){
        //NOTE:Find Record to be shown
        $id = $request->id;
        $crud_listtoindiv = Crud::find($id);
        //NOTE: Return to DetailedView with said Record to be shown
        return view('crud.CrudDetailedView')->with('crud_listtoindiv',$crud_listtoindiv);
    }

    //NOTE: List Page with READ ready
    public function List(Request $request){
        //NOTE:Find ALL record to be shown and redirect to LIST Page with said records
        $sesh = $request->session()->all();
        $crud_reads = Crud::All();
        return view('crud.CrudList')->with('crud_reads',$crud_reads)->with('sesh',$sesh);
    }

    //NOTE: Open the create page
    public function Create(){
        return view('crud.CrudCreate');
    }

    //NOTE: Create/Inpt Process
    public function CreatePost(crudreq $request){
        //NOTE:Proses Create 
        //NOTE:Define Image
        $file  = $request->image;
        $photo = time().str_slug($file->getClientOriginalName(),'_').'.'.$file->getClientOriginalExtension();
        
        //NOTE:Insert Into with Models
        Crud::create([
            'name'       => $request->name,
            'address'    => $request->address,
            'phone'      => $request->phone,
            'kota'      => 'aye',
            'image'     => $photo
        ]);

        //NOTE:Move Image
        $path = public_path('storage/crud/');
        $file->move($path,$photo);

        //NOTE: Redirect ke List
        return redirect()->route('crud.CrudList');
    }

    //NOTE: Edit page
    public function Edit(Request $request){
        //NOTE:Find ID to be edited
        $crud_readtoedit = Crud::findOrfail($request->id);
        //NOTE:Return to Edit view with Value to be Edited
        return view('crud.CrudEdit')->with('crud_readtoedit',$crud_readtoedit);
        // return $crud_readtoedit;
    }

    //NOTE: EDIT PROCESS
    public function EditPost(Request $request){
        //NOTE: Update process with Models
        $crudupdate=Crud::findOrFail($request->id);
        $crudupdate->update([
            'name'       => $request->name,
            'address'    => $request->address,
            'phone'      => $request->phone,
            'kota'      => 'aye'
            // 'image'     => $photo
        ]);
        //NOTE: Image Update Process
        if($request->image != null){
            //NOTE: Find Image and Delete
            $path = public_path('storage/crud/');
            \File::delete($path.$crudupdate->image);

            //NOTE: Define new IMAGE
            $file = $request->image;
            $photo = time().str_slug($file->getClientOriginalName(),'_').'.'.$file->getClientOriginalExtension();
            
            //NOTE: Upload and MOve the new Images with Models
            $crudupdate->update([
                'image' => $photo
            ]);
            $file->move($path,$photo);
        }

        Session::flash('message', 'Successfully killed');
        return redirect()->route('crud.CrudList');
    }

    public function DeletePost(Request $request){
        // Crud::where('name',$name)->firstOrfail();        //NOTE:Find String to be deleted
        $crud_readtodelete = Crud::findOrfail($request->id);//NOTE:Find ID to be deleted
        //NOTE: Check if Image exist, if do Delete
        if($crud_readtodelete->image != null){
            //NOTE: Find Image and Delete
            $path = public_path('storage/crud/');
            \File::delete($path.$crud_readtodelete->image);
        }
        $crud_readtodelete->delete();                       //NOTE:DELETE said ID
 
        return redirect()->route('crud.CrudList');          //NOTE:Redirect to list
    }


    public function abc($id){
        //NOTE: find Post to be Commented on
        $crudment=Crud::findOrFail($id);
        $view=[
        'data' => $crudment
        ];
        //NOTE:Return to COmment Page with new value
        return view('crud.CrudMentar')->with($view);
    }


    public function Store_Comment(Request $request, $id){
        //NOTE: insert into comment
        Comment::create([
            'crud_id' => $id,
            'comment' => $request->comment
        ]);
        
        //NOTE:Return to CrudList
        return redirect()->route('crud.CrudList');
    }
}
