<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class crudreq extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->isMethod('post')){
            return [
                'name' => 'string|required',
                'address' => 'string|required',
                'phone' => 'numeric|required'
            ];
        }
    }

    public function messages(){
        $messages = [
            'name.string' => 'Nama Lengkap Harus string',
            'name.required' => 'Nama Harus diisi',
            'address.string' => 'Alamat Harus string',
            'address.required' => 'Alamat Harus diisi',
            'phone.numeric' => 'Telpon Harus Nomor',
            'phone.required' => 'Telpon Harus diisi',
        ];
    return $messages;
    }
}
