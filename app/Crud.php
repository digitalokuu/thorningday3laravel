<?php

namespace App;
use Image;
Use App\Comment;
use Illuminate\Database\Eloquent\Model;

class Crud extends Model
{

    //NOTE: Table Definer
    protected $table = 'crud';
    protected $fillable = [
        'id',
        'name',
        'address',
        'phone',
        'kota',
        'image',
        'created_at',
        'updated_at'

    ];

    protected $dates = [
        'created_at',
        'updated_at'
    ];

    public $timestamps = true;

    //NOTE: FIND RELATION BETWEEN TABLE AND COLUMNS
    public function comments(){
        return $this->hasMany(Comment::class, 'crud_id');
    }
}
