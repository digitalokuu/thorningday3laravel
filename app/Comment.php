<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    //NOTE: Table Definer
    protected $table = 'comment';
    protected $fillable = [
        'id',
        'crud_id',
        'comment',
        'created_at',
        'updated_at'
    ];

    protected $dates = [
        'created_at',
        'updated_at'
    ];

    public $timestamps = true;

    //NOTE:Relation

 
}
