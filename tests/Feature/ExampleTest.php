<?php

namespace Tests\Feature;
use App\Crud;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        //NOTE: PipeLine PHPUnitTesting 
        $test = Crud::create([
            'name'       => 'Arham',
            'address'    => 'Jalan',
            'phone'      => '908070605040302010',
            'kota'      => 'Samarinda',
            'image'     => 'test.png'
        ]);
        if(isset($test)){
            $this->assertTrue(true);
        }else{
            $this->assertFalse($test);
        }
    }
}
