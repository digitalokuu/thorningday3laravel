<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body> 
        
        <h4>detailed view of {{$crud_listtoindiv->name}}</h4>
        <a href="{{ route('crud.CrudCreate') }}">New Entry</a>
        <a href="{{ route('crud.CrudList') }}">Return to Home</a><br><br>
        id       : {{$crud_listtoindiv->id}} <br>
        name     : {{$crud_listtoindiv->name}} <br>
        address  : {{$crud_listtoindiv->address}} <br>
        phone    : {{$crud_listtoindiv->phone}} <br>
        <img src="{{ asset('storage/crud/'.$crud_listtoindiv->image) }}" alt="">
        <br><br><br>
        <h4><a href="{{ route('crud.CrudMentar',$crud_listtoindiv->id)}}">Commentudo</a></h4>

        <!-- {{$crud_listtoindiv->comments}} -->
        @foreach($crud_listtoindiv->comments as $comment)
        <hr>
        {{$comment->comment}} <br>
        {{$comment->created_at->format('D-M-Y')}} <br>
        <hr>
        @endforeach
</body>
</html>