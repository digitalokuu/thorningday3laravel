<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h4>Welcome to Edit</h4>
    <a href="{{ route('crud.CrudCreate') }}">New Entry</a>
    <a href="{{ route('crud.CrudList') }}">Return to Home</a><br><br>
    <br>
    <form action="{{ route('crud.CrudEditPost') }}" method="post"  enctype="multipart/form-data">
        {{csrf_field()}}
        <input type="hidden" name="id" id="id" value="{{ $crud_readtoedit->id }}"><br>

        <label for="name">Name</label>
        <input type="text" name="name" id="name" value="{{ $crud_readtoedit->name }}"><br>
        
        <label for="address">address</l\abel>
        <input type="text" name="address" id="address" value="{{ $crud_readtoedit->address }}"><br>

        <label for="phone">phone</label>
        <input type="number" name="phone" id="phone" value="{{ $crud_readtoedit->phone }}"><br>

        
        <img src="{{ asset('storage/crud/'.$crud_readtoedit->image) }}" alt=""><br>
        <input type="file" name="image" id="image"><br>

        <input type="submit" value="update">
    </form>
</body>
</html>