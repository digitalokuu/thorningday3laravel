<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <h1><li>{{ $error }}</li></h1>
            @endforeach
        </ul>
    </div>
@endif
    <h4>Welcome to Create</h4>
    <a href="{{ route('crud.CrudList') }}">Return to Home</a><br><br>

    <br>
    <form action="{{ route('crud.CrudCreatePost') }}" method="post" enctype="multipart/form-data">
        {{csrf_field()}}
        <label for="name">Name</label>
        <input type="text" name="name" id="name"><br>
        
        <label for="address">address</label>
        <input type="text" name="address" id="address"><br>

        <label for="phone">phone</label>
        <input type="number" name="phone" id="phpne"><br>

        <label for="phone">Photo</label>
        <input type="file" name="image" id="image"><br>
        <input type="submit" value="submit">
    </form>
</body>
</html>