<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTabelConnect extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        If(!Schema::HasTable('comment')){
            Schema::create('comment', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('crud_id')->unsigned();
                $table->foreign('crud_id')
                        ->references('id')->on('crud')
                        ->onDelete('cascade')
                        ->onUpdate('cascade');
                $table->text('comment');
                $table->timestamps();
                $table->index('crud_id');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comment');
    }
}
