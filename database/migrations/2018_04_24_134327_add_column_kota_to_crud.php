<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnKotaToCrud extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('crud','kota')){
            Schema::table('crud',function(Blueprint $table){
                $table->string('kota');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('crud','kota')){
            Schema::table('crud',function(Blueprint $table){
                $table->dropColumn('kota');
            });
        }
    }
}
