<?php

use Illuminate\Database\Seeder;
use App\Crud;
class crudseed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Crud::create([
            'name' => 'asary',
            'address' => 'samarinda',
            'phone' => '082255271673'
        ]);
    }
}
